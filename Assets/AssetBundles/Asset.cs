﻿
public class Asset
{
    public string id;
    public string url;

    public Asset(string id, string url)
    {
        this.id = id;
        this.url = url;
    }
}