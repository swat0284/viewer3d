﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class BundleMenager : MonoBehaviour {
    public Object[] k;
    void Start()
    {
        StartCoroutine(GetAssetBundle());
    }

    IEnumerator GetAssetBundle()
    {
        UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle("file:///Users/swat/Desktop/Zlecenia/Własne/3dVR/AssetBundles/Android/mieszkanie1");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.isDone);
            AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www);
            k = bundle.LoadAllAssets();
            foreach (Object o in k)
            {
                Instantiate(o);
            }
           
        }
    }
}
