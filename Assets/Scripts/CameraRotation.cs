﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour {

	// Use this for initialization
	void Start () {
        InvokeRepeating("Rotate",2.0f,2.0f);
	}
    public void Rotate()
    {

        transform.Rotate(0, 2, 0);
    }
    private IEnumerator RotateCamera(float increment)
    {
      
                transform.Rotate(0, increment, 0);
        return null;
    }


	// Update is called once per frame
	void Update () {
		
	}
}
