﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;
public class FirebaseManager : MonoBehaviour {

    void Start()
    {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://wirtualna-rzeczywiscosc.firebaseio.com/");
    }

  
	// Update is called once per frame
    public void SaveDataOnline (string userId, string name, string email) {
        Debug.Log("Save data");
        Asset asset = new Asset(name, email);
            string json = JsonUtility.ToJson(asset);

        FirebaseDatabase.DefaultInstance.RootReference.Child("Assets").Child(userId).SetRawJsonValueAsync(json);


	}

    public void GetData(string id, Action<Asset> callbackFunction)
    {
        Debug.Log("Load data");
        Asset tempPlayerData = null;
        FirebaseDatabase.DefaultInstance
                        .GetReference("Assets").Child(id)
      .GetValueAsync().ContinueWith(task => {
        if (task.IsFaulted)
        {
                Debug.LogError(task.Exception);
              
        }
        else if (task.IsCompleted)
        {
                DataSnapshot snapshot = task.Result;
                 tempPlayerData = JsonUtility.FromJson<Asset>(snapshot.GetRawJsonValue());
                callbackFunction(tempPlayerData);
                Debug.Log(tempPlayerData.url);
             
        }
    });
        //return tempPlayerData;

    }

}