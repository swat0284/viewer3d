﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepContainter : MonoBehaviour {
    GameObject hiddenObject;
	// Use this for initialization
	public void HideObject (GameObject obj,float time) {
        hiddenObject = obj;
        StartCoroutine(DisableObject(time));
	}
    IEnumerator DisableObject(float time)
    {
        yield return new WaitForSeconds(time);
        hiddenObject.SetActive(false);
    }
    public void ShowObject()
    {
        if (hiddenObject)
        {
            hiddenObject.SetActive(true);
            hiddenObject.GetComponent<Animator>().Play("FadeInAnimation");
        }
    }
}
