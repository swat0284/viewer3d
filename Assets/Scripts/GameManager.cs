﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public FirebaseManager firebaseManager;
    public Asset assetData;
    Action<Asset> loadedAction = AssetDataLoad;

    public static GameManager instance = null;             
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
        InitGame();
    }

   public void TryToLogin(string login)
    {
        firebaseManager.GetData(login, loadedAction);
    }

    static void AssetDataLoad(Asset asset)
    {
        Debug.Log("DataLoaded" + asset.url);
        //    var assetDownloader = GetComponent<AssetDownloader>();
        //    assetDownloader.DownloadAsset(asset.url, ".fbx", LoadDownloadedAsset, null, GetAssetLoaderOptions());
    }
    private AssetLoaderOptions GetAssetLoaderOptions()
    {
        var assetLoaderOptions = AssetLoaderOptions.CreateInstance();
        assetLoaderOptions.DontLoadCameras = false;
        assetLoaderOptions.DontLoadLights = false;
        assetLoaderOptions.UseCutoutMaterials = true;//_cutoutToggle.isOn;
        assetLoaderOptions.AddAssetUnloader = true;
        return assetLoaderOptions;
    }

    private void LoadDownloadedAsset(GameObject loadedGameObject)
    {
        /* PreLoadSetup();
         if (loadedGameObject != null)
         {
             _rootGameObject = loadedGameObject;
             PostLoadSetup();
         }
         else
         {
             var assetDownloader = GetComponent<AssetDownloader>();
             ErrorDialog.Instance.ShowDialog(assetDownloader.Error);
         }
         */
    }
    void InitGame()
    {

    }

}
