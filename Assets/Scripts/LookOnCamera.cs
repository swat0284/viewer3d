﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookOnCamera : MonoBehaviour {
    Transform camera;
	// Use this for initialization
	void Start () {
        camera = Camera.main.transform;
	}

    // Update is called once per frame
    void Update()
    {
        if (camera)
        {
            Vector3 posCam = camera.position - transform.position;
            posCam.y = 90;
            Quaternion lastPos = Quaternion.LookRotation(-posCam);
            transform.rotation = Quaternion.Slerp(transform.rotation, lastPos, Time.deltaTime * 3);
        }
    }
}
