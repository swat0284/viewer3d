﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleARCore;
using System;
using TriLib;
using UnityEngine.SceneManagement;
public class MaimMenuManager : MonoBehaviour
{
    public GameObject buttonVR;
    public GameObject buttonAR;
    public GameObject buttonX;
    public InputField inputField;

    public ToastMessage toastMessage;

    public enum mode
    {
        VR,
        AR,
        X
    };
    mode AppMode;
    public void ButtonARClick()
    {
        StartCoroutine(CheckCompatibility());
        //SelectScene(mode.AR);
    }
    private IEnumerator CheckCompatibility()
    {
        AsyncTask<ApkAvailabilityStatus> checkTask = Session.CheckApkAvailability();
        CustomYieldInstruction customYield = checkTask.WaitForCompletion();
        yield return customYield;
        ApkAvailabilityStatus result = checkTask.Result;
        switch (result)
        {
            case ApkAvailabilityStatus.SupportedApkTooOld:
                toastMessage.showToastOnUiThread("Zaktualizuj aplikacje");
                Session.RequestApkInstallation(false);
                break;
            case ApkAvailabilityStatus.SupportedInstalled:
                SelectScene(mode.AR);
                break;
            case ApkAvailabilityStatus.SupportedNotInstalled:
                toastMessage.showToastOnUiThread("Zainstaluj pakiet ARCore");
                Session.RequestApkInstallation(false);

                break;
            case ApkAvailabilityStatus.UnknownChecking:
                toastMessage.showToastOnUiThread("Nieznany błąd");
                break;
            case ApkAvailabilityStatus.UnknownError:
                toastMessage.showToastOnUiThread("Nieznany błąd");
                break;
            case ApkAvailabilityStatus.UnknownTimedOut:
                toastMessage.showToastOnUiThread("Timed out");
                break;
            case ApkAvailabilityStatus.UnsupportedDeviceNotCapable:
                toastMessage.showToastOnUiThread("Urządzenie niewspierane. Przepraszamy");
                break;
        }
    }
    public void ButtonVRClick()
    {
        SelectScene(mode.VR);
    }
    public void ButtonXClick()
    {
        SelectScene(mode.X);
    }
    public void SelectScene(mode applicatinMode)
    {
        switch (applicatinMode)
        {
            case mode.VR:
                buttonAR.GetComponent<Animator>().Play("ButtonAlfaOut");
                buttonX.GetComponent<Animator>().Play("ButtonAlfaOut");
                buttonVR.GetComponent<Animator>().Play("FlipButton");
                StartCoroutine(LoadScene(1.1f, 1));
                break;
            case mode.AR:
                buttonVR.GetComponent<Animator>().Play("ButtonAlfaOut");
                buttonX.GetComponent<Animator>().Play("ButtonAlfaOut");
                buttonAR.GetComponent<Animator>().Play("FlipButton");
                StartCoroutine(LoadScene(1.1f, 2));
                break;
            case mode.X:
                buttonAR.GetComponent<Animator>().Play("ButtonAlfaOut");
                buttonVR.GetComponent<Animator>().Play("ButtonAlfaOut");
                buttonX.GetComponent<Animator>().Play("FlipButton");
                StartCoroutine(LoadScene(1.1f, 3));
                break;
        }
    }

    IEnumerator LoadScene(float offset, int index)
    {
        yield return new WaitForSeconds(offset);
        //  AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(index);

        // Wait until the asynchronous scene fully loads
        // while (!asyncLoad.isDone)
        // {
        //   yield return null;
        // }
        Application.LoadLevel(index);
    }
    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }
    public void EnableVR()
    {
        UnityEngine.XR.XRSettings.enabled = true;
    }

    public void LoginPlayer()
    {
        //assetData = 
        GameManager.instance.TryToLogin(inputField.text);
       // Debug.Log(assetData.url);
    }
	void Start () {
        UnityEngine.XR.XRSettings.enabled = false;
	}
}
