﻿using System;
using System.Collections;
using UnityEngine;
using VRStandardAssets.Common;
using VRStandardAssets.Utils;

namespace VRStandardAssets.ShootingGallery
{
    // This script handles a target in the shooter scenes.
    // It includes what should happen when it is hit and
    // how long before it despawns.
    public class MoveToPointItem : MonoBehaviour
    {
        [SerializeField] private SelectionSlider m_SelectionSlider;
        public VRMovment vrmovment;
        FootstepContainter footstepContainter;
        public Animator animator;
        public void Start()
        {
            footstepContainter = GameObject.Find("FootstepContainter").GetComponent<FootstepContainter>();
        }
        private void OnEnable()
        {
            m_SelectionSlider.OnBarFilled += HandleBarFilled;
            m_SelectionSlider.OnClick += HandleBarFilled;
        }


        private void OnDisable()
        {
            m_SelectionSlider.OnBarFilled -= HandleBarFilled;
            m_SelectionSlider.OnClick -= HandleBarFilled;
        }

        private void HandleBarFilled()
        {
            if (!vrmovment)
                vrmovment = GameObject.Find("CameraContainer").GetComponent<VRMovment>();
            // When the selection slider is filled, the power should be turned off.
            Debug.Log("filled");
            footstepContainter.ShowObject();
            footstepContainter.HideObject(this.gameObject,1.0f);
            animator.Play("FadeOutAnimatin");
            vrmovment.TryToMovePlayer(this.transform.position);
        }
    }
}
