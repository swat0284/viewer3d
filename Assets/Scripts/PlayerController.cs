﻿using UnityEngine;
using UnityEngine.UI;
public class PlayerController : MonoBehaviour
{
    public Text eyes;
    Transform oka;
    public UpdateEyeAnchors ey;
    void Update()
    {
      
        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 3.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;
        // this.transform.rotation = eyes.eyes[0].transform.rotation;
        //transform.Rotate(0, x, 0);
        Vector3 desiredMove = Camera.main.transform.forward * x + Camera.main.transform.right * z;
        transform.Translate(desiredMove);
    }
}