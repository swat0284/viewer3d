﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RotateObjectByTouch : MonoBehaviour {

    private float rotationRate = .1f;
    Touch myTouch;
   
    void Update()
    {

        foreach (Touch touch in Input.touches)
        {
            myTouch = touch;
            Debug.Log("Touching at: " + touch.position);

            if (touch.phase == TouchPhase.Began)
            {
                Debug.Log("Touch phase began at: " + touch.position);
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                Debug.Log("Touch phase Moved");
                if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
                {
                    transform.Rotate(0,
                                     touch.deltaPosition.x * rotationRate, 0, Space.World);
                }
                if (Input.deviceOrientation == DeviceOrientation.LandscapeRight)
                {
                    transform.Rotate(0,
                                     -touch.deltaPosition.x * rotationRate, 0, Space.World);
                }
               
                if (Input.deviceOrientation == DeviceOrientation.Portrait)
                {
                    transform.Rotate(0,
                                     touch.deltaPosition.y * rotationRate, 0, Space.World);
                }
               
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                Debug.Log("Touch phase Ended");
            }
        }
    }

  









}