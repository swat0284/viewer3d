﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;
using UnityEngine.SceneManagement;
public class VRManager : MonoBehaviour {
    public GameObject helpButton;
    public GyroscopeMovment gyroscopeMovment;
    public TouchMovment touchMovment;

    public bool isVR = true;
	// Use this for initialization
	void Start () {
        UnityEngine.XR.XRSettings.enabled = isVR;
        if(Application.loadedLevelName != "AR" )
        StartCoroutine(LoadLevel());
    }
    IEnumerator LoadLevel()
    {
        enabled = false;
        yield return SceneManager.LoadSceneAsync(
            4, LoadSceneMode.Additive
        );
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(4));
        enabled = true;
        LoadIsDone();

    }
    public void LoadIsDone()
    {
        helpButton.GetComponent<Animator>().Play("BlueBoxOut");
    }
    public void GyroscopeMovmentEnable()
    {
        gyroscopeMovment.enabled = true;
    }
    public void TouchMovmentEnable()
    {
        touchMovment.enabled = true;
    }
    public void LoadMenu()
    {
        helpButton.GetComponent<Animator>().Play("BlueBoxIn");
        Application.LoadLevel(0);
    }
    public bool loading = false;
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape) && !loading)
        {
            loading = true;
            LoadMenu();
    }
    }
}
