﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRMovment : MonoBehaviour {
    private float journeyLength;
    // Movement speed in units/sec.
    public float speed = 1.0F;

    // Time when the movement started.
    private float startTime;
	// Update is called once per frame
    public void TryToMovePlayer(Vector3 point)
    {
        StopAllCoroutines();
        startTime = Time.time;

        // Calculate the journey length.
        journeyLength = Vector3.Distance(transform.position, point);
       
        point = new Vector3(point.x,transform.position.y,point.z);
        StartCoroutine(MovePlayerToPoint(transform.position,point));
    }
    IEnumerator MovePlayerToPoint(Vector3 startPoint, Vector3 endPoint)
    {
        while (true)
        {
           
            float distCovered = (Time.time - startTime) * speed;

            // Fraction of journey completed = current distance divided by total distance.
            float fracJourney = distCovered / journeyLength;

            // Set our position as a fraction of the distance between the markers.
            transform.position = Vector3.Lerp(startPoint, endPoint, fracJourney);
            yield return null;
        }     
    }
}

