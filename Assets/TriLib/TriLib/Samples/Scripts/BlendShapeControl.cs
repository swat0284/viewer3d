﻿using UnityEngine;
using UnityEngine.UI;

namespace TriLib
{
    namespace Samples
    {
        public class BlendShapeControl : MonoBehaviour
        {
            /// <summary>
            /// Blend-shape name <see cref="UnityEngine.UI.Text"/> reference.
            /// </summary>
            [SerializeField]
            private Text _text;

            /// <summary>
            /// Blend-shape weight <see cref="UnityEngine.UI.Slider"/> reference.
            /// </summary>
            [SerializeField]
            private Slider _slider;

            /// <summary>
            /// Gets/Sets the related UI component text.
            /// </summary>
            public string Text
            {
                get
                {
                    return _text.text;
                }
                set
                {
                    _text.text = value;
                }
            }

            /// <summary>
            /// <see cref="UnityEngine.SkinnedMeshRenderer"/> assigned to the blend-shape.
            /// </summary>
            public SkinnedMeshRenderer SkinnedMeshRenderer;

            /// <summary>
            /// Blend-shape index.
            /// </summary>
            public int BlendShapeIndex;

            /// <summary>
            /// Changes the loaded model blend-shape value accordingly.
            /// </summary>
            /// <param name="value">Blend weight.</param>
            public void OnValueChange(float value)
            {
                AssetLoaderWindow.Instance.HandleBlendEvent(SkinnedMeshRenderer, BlendShapeIndex, value);
            }
        }
    }
}