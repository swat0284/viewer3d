﻿#if !UNITY_EDITOR && !MONO
using System;
namespace AOT {
    ///<summary>
    ///Attribute used to annotate functions that will be called back from the unmanaged world.
    ///</summary>
    public class MonoPInvokeCallbackAttribute : Attribute
    {
        public MonoPInvokeCallbackAttribute (Type delegateType) { }
    }
}
#endif